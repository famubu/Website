---
title: Contribute
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
published: true
---

![](bug.png?resize=45,45&classes=pull-left)
# Чем я могу помочь?

Есть много способов внести свой вклад в проект **Disroot**.
