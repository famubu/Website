---
title: 'Feuille de route'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

# Communauté

---      

**La transparence** joue un grand rôle sur **Disroot**.
Nous voulons être transparents sur nos plans, nos idées et nos progrès actuels.

Nous voulons également encourager les contributions au projet de la communauté **Disroot**.
Par conséquent, nous avons mis en place de nombreux outils pour faciliter le suivi du développement de **Disroot** pour tous, et permettre la participation de tous aux discussions, aux signalements d'erreurs ou de bugs, ainsi qu'à la soumission d'idées et de corrections.
