---
title: Bitcoin
media_order: '1GN.png,qr0719.png'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
---

![](morecats-bitcoin.png)

---

# Bitcoin
Invia i tuoi bitcoin al nostro portamonete:
bc1q2r5ufc68unc8g8na57sufjgcjpvgvlztnswgw3
