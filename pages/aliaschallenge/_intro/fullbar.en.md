---
title: 'Introduction'
bgcolor: '#7b384fff'
fontcolor: '#FFF'
text_align: left
---

<br>

<span style="padding-right:10%;" id="fund1">
In the last days of this year, when the entire world prepares to greet the light of the new decade, three domains are fighting for the dominance of the beetroot market. **Join the battle and choose your Overlord**.
</span>

<span style="padding-right:30%;" id="fund2">
From 23rd of December until 1st of January, take part in our end of the year fundraising event. **Donate to Disroot** using one of the below domain hashtags as your donation reference. Victorious domain name will be given as an **extra alias for all Disrooters now and in the future**.
</span>

## Let the coolest domain win!
