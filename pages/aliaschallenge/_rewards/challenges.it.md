---
title: 'Ricompensa'
bgcolor: '#f2f2f2ff'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: "Dona 30€"
        text: "Ottieni un biglietto di auguri personale per il nuovo anno."
    -
        title: "Dona 75€"
        text: "Una sorpresa speciale per il nuovo anno nella tua casella di posta."
    -
        title: "Dona 100€"
        text: "Scegli una nuova immagine per la prima pagina di Disroot.org per due settimane."
    -
        title: "Dona 500€"
        text: "Seleziona un nuovo nome di dominio per un alias Disroot personalizzato da assegnare a tutti i Disrooter."


---

<div markdown=1>
`*` Disroot.org si riserva il diritto di negare il premio se l'immagine o il nome violano i nostri <a href="/tos"> Termini di servizi.</a>
</div>
