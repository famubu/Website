---
title: 'Rewards'
bgcolor: '#f2f2f2ff'
fontcolor: '#327E82'
text_align: left
clients:
    -
        title: "Donate 30€"
        text: "Get a personal new year's greeting card."
    -
        title: "Donate 75€"
        text: "A New years special surprise heading your mailbox."
    -
        title: "Donate 100€"
        text: "Choose a new Image for the Disroot.org front page for two weeks.*"
    -
        title: "Donate 500€"
        text: "Select a new domain name for a custom Disroot alias given to all Disrooters.*"


---

<div markdown=1>
`*` Disroot.org reserve the right to deny the reward if image or name are in violation of our <a href="/tos"> TOS</a>
</div>
