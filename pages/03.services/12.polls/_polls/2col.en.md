---
title: Polls
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://poll.disroot.org/">Create a poll</a>

---

![](framadate_logo.png)

**Disroot**'s Polls are powered by **Framadate**. Framadate is an online service for planning an appointment or making a decision quickly and easily. Create your poll, share it with your friends or colleague so that they can participate in the decision process and get the results!

You don't need any account on **Disroot** to use this service.

Disroot Poll: [https://poll.disroot.org](https://poll.disroot.org)

Source code: [https://git.framasoft.org/framasoft/framadate](https://git.framasoft.org/framasoft/framadate)
