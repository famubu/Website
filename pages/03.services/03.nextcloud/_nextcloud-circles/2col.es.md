---
title: 'Círculos Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](en/nextcloud-circles.png?lighbox=1024)

---

## Círculos

Crea tus propios grupos de usuarixs/colegas/amigxs para compartir fácilmente con ellxs. Tus círculos pueden ser públicos, privados (requiere invitación aunque es visible para otrxs) o secreto (requiere contraseña y es invisible).
