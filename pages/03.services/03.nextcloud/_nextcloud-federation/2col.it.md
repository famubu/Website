---
title: 'Federazione di Nextcloud'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
---

## Federazione

Grazie alla federazione puoi collegare e condividere le tue cartelle con chiunque usi un'istanza di **Nextcloud** o **ownCloud** anche fuori da **Disroot**.

---

![](en/NC_federation.png?lightbox=1024)
