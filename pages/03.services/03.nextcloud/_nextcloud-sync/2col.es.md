---
title: 'Nextcloud Sync'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
cloudclients: true
clients_title: 'Elige tu cliente favorito'
clients:
    -
        title: Escritorio
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-linux, fa-windows, fa-apple]
    -
        title: Móvil
        logo: en/nextcloud_logo.png
        link: https://nextcloud.com/install/#install-clients
        text:
        platforms: [fa-android, fa-apple]
    -
        title: Navegador
        logo: en/webbrowser.png
        link: https://cloud.disroot.org
        text: 'Accede directamente desde tu navegador'
        platforms: [fa-linux, fa-windows, fa-apple]

---

## Sincronización Nextcloud

Accede y organiza tu información desde cualquier plataforma. Utiliza los clientes para el **Escritorio**, **Android** o **iOS** para trabajar con tus archivos sin parar, o sincroniza continuamente tus carpetas favoritas entre tu ordenador de escritorio y tu laptop.

---

![](en/nextcloud-files.png?lightbox=1024)
