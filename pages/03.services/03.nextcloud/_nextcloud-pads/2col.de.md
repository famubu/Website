---
title: 'Nextcloud Pads'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Gemeinschaftliche Schriftstücke


Erstelle Schriftstücke oder Tabellen direkt aus **Nextcloud** und verfolge ihre Entwicklung.

**HINWEIS! Die Schriftstücke und Tabellen sind Teil eines anderen Service und ihr Inhalt wird nicht in der Cloud gespeichert. Auf die erstellten Schriftstücke kann theoretisch jeder zugreifen, der die zu ihnen führende, zufällige URL "errät".**

---

![](de/OCownpad.png?lightbox=1024)
