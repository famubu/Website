---
title: 'Spazio di archiviazione extra'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: left
section_id: storage
---

## Spazio di archiviazione extra

È possibile estendere lo spazio di archiviazione a 14,29 o 54 GB al costo di 0.15 Euro per GB per mese. È possibile utilizzare lo spazio extra pure per i tuoi messaggi di posta elettronica. Se fossi interessato a questa opzione, ti chiediamo di specificarlo nella sezione commenti della form da compilare.

Per richiedere spazio aggiuntivo compliare questa [form](/forms/extra-storage-space).<br>


---

<br><br>

<a class="button button1" href="https://disroot.org/it/forms/extra-storage-space">Richiedi spazio extra</a>
