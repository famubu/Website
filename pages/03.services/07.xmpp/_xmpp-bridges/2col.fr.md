---
title: 'Passerelles Xmpp'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: right
---

![](conversations.jpg)

---

## Passerelles, Bridges et Transports

XMPP permet une grande variété de moyens de connexion aux protocoles de chats. Vous pouvez également vous connecter à n'importe quel channel IRC via notre passerelle IRC Biboumi.. Nous sommes en train de penser à mettre en service un bridge/passerelle Matrix (disponible à présent via bau-haus.net, nos amis onlines), un bridge/passerelle Telegram ainsi que plein d'autres passerelles en préparation par nos soins.  
