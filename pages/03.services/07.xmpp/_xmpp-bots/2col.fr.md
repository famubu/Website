---
title: 'Xmpp Bots'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

Les Bots sont des comptes sur les serveurs de chats qui font des choses à votre place. Que ce soit vérifier la météo, accueilir les nouveaux arrivants, se souvenir de choses pour vous, faire des recherches en ligne ou même vous fournir des feeds rss et encore une panoplie d'autres actions utiles (ainsi que d'autres encore qui ne servent à rien). Nous offrons une quantité de ces robots multi-usages basés sur **hubot** ainsi qu'un serveur bot dédié appelé `bot.disroot.org`. Si vous avez un bot et que vous cherchez un endroit d'accueil pour celui-ci, contactez-nous.<br>

Notre serveur bot ne garde aucun historique.  

---

![](hubot_logo.png)
