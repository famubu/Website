---
title: 'Xmpp Bots'
bgcolor: '#FFF'
fontcolor: '#555'
text_align: left
wider_column: left
---

## Bots

I bot sono account che eseguono dei comandi per te: comunicarti la temperatura in una città, salutare i nuovi account connessi, ricordarti cose, fare ricerche sul web oppure fornirti feed/rss o moltissime altre cose più o meno utili.

Offriamo un numero di bot multiuso basato su hubot e un server bot dedicato chiamato bot.disroot.org.

Se hai un bot e stai cercando una casa per lui, mettiti in contatto con noi.

Il nostro bot server non conserva alcuna cronologia.

---

![](hubot_logo.png)
