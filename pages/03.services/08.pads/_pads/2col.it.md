---
title: Pads
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://pad.disroot.org/">Apri un pad</a>
<a class="button button1" href="https://calc.disroot.org/">Crea un nuovo foglio di calcolo</a>

---

![](etherpad.png)
## Editor collaborativo in tempo reale

Etherpad permette di editare documenti in modo collaborativo e in modo sincrono direttamente dall'interfaccia web.
Scrivi in modo collettivo e sincrono con i tuoi amici o colleghi un articolo, un comunicato stampa o una to-do list.


[https://pad.disroot.org](https://pad.disroot.org)

Pagina del progetto: [http://etherpad.org](http://etherpad.org)

[Codice sorgente](https://github.com/ether/etherpad-lite)


![](ethercalc.png)
## Un foglio di calcolo collaborativo.
I dati sono salvato nel web e le persone possono modificare lo stesso documento nello stesso momento. Tutte le modifiche effettuate vengono immediatamente visualizzate sullo schermo.

Lavora in modo collaborativo sul foglio di calcolo!

[https://calc.disroot.org](https://calc.disroot.org)

Pagina del progetto: [https://ethercalc.net](https://ethercalc.net)
[Codice sorgente(https://github.com/audreyt/ethercalc)
