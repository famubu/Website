---
title: Blocs
bgcolor: '#FFF'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _pads
            - _etherpad-highlights
            - _etherpad-features
            - _ethercalc-highlights
            - _ethercalc-features
            - _empty-bar
body_classes: modular
header_image: 'riot-tagging.jpeg'
---
