---
title: 'Ethercalc Funktionen'
text_align: left
wider_column: right
---


![](de/etherpad-history.gif)

---

## Pads Importieren / Exportieren

Importiere und Exportiere Pads in einer Vielzahl von Formaten (plain-text, HTML, Etherpad, Mediawiki).

## Verlaufsspeicher

Speichere verschiedene Versionen Deines Pads und mache Änderungen rückgängig.

## Lesezeichen

Speichere eine Liste mit Lesezeichen der von Dir besuchten Pads, lokal im Speicher Deines Browsers.

## Kommentare

Über eine Seitenleiste kannst Du Kommentare hinzufügen, die direkt mit dem Text verknüpft sind.
