---
title: Recherche
bgcolor: '#FFF'
fontcolor: '#1F5C60'
body_classes: modular
wider_column: right
---

<a class="button button2" href="https://search.disroot.org">Aller sur Searx</a>
<a class="button button1" href="http://mycroftproject.com/search-engines.html?name=disroot.org">Installer le plugin de navigateur</a>

---

![](searx_logo.png)

# Plate-forme multi moteur de recherche anonyme

Disroot's Search est un moteur de recherche comme Google, DuckDuckGo, Qwant, powered by **Searx**. Ce qui le rend unique par rapport aux autres est qu'il s'agit d'un métamoteur de recherche, agrégeant les résultats d'autres moteurs de recherche sans stocker d'informations sur ses utilisateurs.

Vous n'avez besoin d'aucun compte sur Disroot pour utiliser ce service.

Disroot Search : [https://search.disroot.org](https://search.disroot.org)

Code source : [https://github.com/asciimoo/searx](https://github.com/asciimoo/searx)
