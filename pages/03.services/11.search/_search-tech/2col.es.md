---
title: Búsqueda
bgcolor: '#FFF'
fontcolor: '#555'
body_classes: modular
wider_column: left
---

# ¿Cómo funciona?

**Searx** no registra ni crea un índice de sitios por sí mismo. Cuando escribes tu consulta en el buscador, **Searx** la deriva a un número de otros buscadores como Google, DuckDuckGo, Bing, etc., y te devuelve los resultados de esos motores en forma agrupada.

**Searx** tal vez no te ofrezca resultados tan personalizados como Google, pero eso es porque no genera un perfil acerca de ti ni comparte tus detalles personales, tu ubicación o la de tu computadora con ningún motor de búsquedas a los que deriva tus consultas. Esto te brinda mucha mejor privacidad y actúa como un 'escudo' contra los grandes motores corporativos que te espían.

Puedes ver [aquí](https://search.disroot.org/preferences) qué motores de búsqueda puedes utilizar para conseguir resultados de tus consultas.
