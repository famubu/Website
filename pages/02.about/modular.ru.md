---
title: 'Мы'
bgcolor: '#fff'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _about-who-and-why
            - _federation
            - _privacy
            - _transparency
body_classes: modular
header_image: about-banner.jpeg
---
