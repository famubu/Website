---
title: privacy
fontcolor: '#555'
wider_column: left
bgcolor: '#fff'

---

# Datenschutz

Software kann erschaffen und geformt werden, wie man das möchte. Jeder Button, jede Farbe und jede Verknüpfung, die wir im Internet sehen, wurde von irgendjemand hingetan. Wenn wir die Anwendungen nutzen, die uns zur Verfügung gestellt werden, sehen wir meistens nicht - oft ist es uns auch egal - was hinter der Benutzeroberfläche passiert. Wir verbinden und mit anderen Menschen, speichern unsere Dateien, organisieren Besprechungen und Festivals, senden Emails oder chatten stundenlang - Und all das ganz einfach auf magische Weise!
In den letzten ein oder zwei Jahrzehnten wurden Informationen immer wertvoller und gleichzeitig immer einfacher zu sammeln und zu verarbeiten. Wir haben uns daran gewöhnt, analysiert zu werden, blindlings Allgemeine Geschäftsbedingungen, Nutzungsbedingungen und Datenschutzerklärungen zu "unserem eigenen Besten" zu akzeptieren, Obrigkeiten und Multimilliarden-Dollar-Unternehmen den Schutz unserer Interessen anzuvertrauen. Dabei sind wir die ganze Zeit nur das Produkt in deren 'Menschenfarmen'.

**Sei der Herr über Deine eigenen Daten:**
Viele Netzwerke nutzen Deine Daten, um Geld damit zu verdienen, Deine Interaktionen zu analysieren und die daraus gewonnenen Informationen zu Werbezwecken (und mehr) zu nutzen. **Disroot** hat kein Interesse an Deinen Daten und nutzt sie für keinen anderen Zweck, als Dir die Verbindung mit unserem Service und dessen Nutzung zu ermöglichn.
Deine Dateien in der Cloud sind verschlüsselt mit Deinem Benutzerpasswort, jeder Wegwerf-Container und jede auf **Lufi** hochgeladene Datei sind von der Nutzerseite aus verschlüsselt. Das bedeutet, dass nicht mal die Server-Administratoren Zugriff auf Deine Daten haben. Wann immer es die Möglichkeit zur Verschlüsselung gibt, aktivieren wir sie, und wenn das nihct möglich ist, raten wir zur Nutzung externer Verschlüsselungssoftware. Je weniger wir, die Administratoren, über Deine Daten wissen, umso besser :D. (Tipp des Tages: *Verliere niemals Dein Passwort!*) Sieh Dir dazu auch [dieses Tutorial](https://howto.disroot.org/de/tutorials/user/account/ussc) an.

---

<br>
![](priv1.jpg?lightbox=1024)
![](priv2.jpg?lightbox=1024)
