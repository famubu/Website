---
title: 'Step 2'
bgcolor: '#1F5C60'
fontcolor: '#fff'
wider_column: right
---

# Step 2:
## Setup your DNS

Once you have your own domain, you need to do two things in order to link it to disroot.org.
*(The following steps are done via interface provided by your domain registrar -adding DNS records-)*:


---      

## 1: Prove you own the domain
Simply create a **TXT** record with: **disroot.org-yourdomain.ltd** (replacing `yourdomain.ltd` with **your domain name**, of course). This will indicate you do in fact own that domain.

## 2: Point your domain to disroot.org
**1st.** Set MX record to **mx01.disroot.org.** **`(don't forget the final dot!)`**
**2nd.** Set **"v=spf1 mx a ptr include:disroot.org -all"** This allows you to send emails from your own servers as well as **Disroot**'s. This is just a default setting that should be applicable to most of the scenarios. If you want to learn more about SPF records and would like to tweak yours, check the web for more info (e.g. [this page](https://www.dmarcanalyzer.com/spf/how-to-create-an-spf-txt-record/)).
