---
title: 'Espace email'
bgcolor: '#1F5C60'
fontcolor: '#FFF'
wider_column: right
---

<br>
Avec votre compte **Disroot**, vous bénéficiez d'1 Go d'espace de stockage GRATUIT pour vos e-mails.

Cependant, il est possible d'étendre ce stockage de vos e-mails de 1 Go **jusqu'à 10 Go** pour le coût de 0,15 euro par Go par mois, payé annuellement (TVA non incluse).

<a class="button button1" href="https://disroot.org/forms/extra-storage-mailbox">Demande d'un espace mail supplémentaire</a>

---      

# Mail storage

![email](logo_email.png?resize=150)
