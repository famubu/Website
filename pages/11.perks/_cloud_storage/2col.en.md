---
title: 'Cloud storage'
bgcolor: '#FFF'
fontcolor: '#555'
wider_column: right
---

# Cloud storage

![cloud](logo_cloud.png?resize=150)

---      

<br>
With your **Disroot** account, you get 2GB of FREE storage.

However, it is possible to extend your cloud storage from 2GB **to 14, 29 or 54GB** for the cost of 0.15 euro per GB per month (VAT not included).

You can also choose to use some or all of the additional space for email storage *(make sure to specify it in the comment section of the request form)*.

<a class="button button1" href="https://disroot.org/forms/extra-storage-space">Request Extra Cloud Storage</a>
