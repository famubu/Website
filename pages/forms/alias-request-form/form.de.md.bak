---
title: 'Alias Request Form'
header_image: introverts.jpg
form:
    name: Alias-request-form
    fields:
        -
            name: username
            label: 'Benutzerame'
            placeholder: 'Trage Deinen Disroot-Benutzernamen ein'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 1'
            label: Alias
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: true
        -
            name: 'Alias 2'
            label: 'Zweiter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 3'
            label: 'Dritter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 4'
            label: 'Vierter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: 'Alias 5'
            label: 'Fünfter Alias (optional)'
            placeholder: Dein Alias
            type: text
            validate:
                pattern: '[A-Za-z0-9.-]*'
                required: false
        -
            name: contribution
            label: 'Spende via'
            placeholder: select
            type: select
            options:
                patreon: Patreon
                paypal: Paypal
                stripe: 'Stripe (creditcard)'
                bank: Banküberweisung
                faircoin: Faircoin
                bitcoin: Bitcoin
            validate:
                required: true
        -
            name: amount
            label: Betrag
            placeholder: 'EUR/USD/BTC/etc.'
            type: text
            validate:
                pattern: '[A-Za-z0-9., ]*'
                required: true
        -
            name: frequency
            label: Zahlweise
            type: radio
            default: monthly
            options:
                monthly: Monatlich
                yearly: Jährlich
            validate:
                required: true
        -
            name: reference
            label: Betreff
            type: text
            placeholder: 'Name des Überweisenden oder andere Tranfer-Referenz'
            validate:
                required: true
        -
            name: comments
            label: Anmerkungen
            type: textarea
        -
            name: honeypot
            type: honeypot
    buttons:
        -
            type: submit
            value: Absenden
        -
            type: reset
            value: Zurücksetzen
    process:
        -
            email:
                from: alias-request@disroot.org
                to: '{{ form.value.username }}@disroot.org'
                subject: '[Disroot] Deine Alias-Anfrage'
                body: '<br><br>Hi {{ form.value.username|e }}, <br><br><strong>Vielen Dank für Deine Spende an Disroot.org!</strong><br>Wir freuen uns sehr über Deine Großzügigkeit.<br><br>Wir werden Deine Alias-Anfrage bearbeiten und uns so schnell wie möglich wieder bei Dir melden.</strong><br><br><hr><br><strong>Hier nochmal eine Zusammenfassung der Anfrage, die wir ehalten haben:</strong><br><br>{% include ''forms/data.html.twig'' %}'
        -
            email:
                from: alias-request@disroot.org
                to: '{{ config.plugins.email.to }}'
                reply_to: '{{ form.value.username }}@disroot.org'
                subject: '[Alias request] - {{ form.value.username|e }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
        -
            message: 'Deine Alias-Anfrage wurde gesendet!'
        -
            display: thankyou
---

<h1 class="form-title"> Email-Alias beantragen </h1>
<p class="form-text"><strong>Aliase sind verfügbar für regelmäßige Unterstützer.</strong> Mit regelmäßigen Unterstützern meinen wir die Community-Mitglieder, die uns mindestens eine Tasse Kaffee im Monat "kaufen".
 <br><br>
Es ist jetzt nicht so, dass wir hier Kaffee anpreisen wollen. Auf der anderen Seite ist Kaffee jedoch eine sehr griffige Analogie für <a href="http://thesourcefilm.com/">Ausbeutung</a> und <a href="http://www.foodispower.org/coffee/">Ungleichheit</a>. Wir denken, das ist eine gute Möglichkeit, jeden selbst bestimmen zu lassen, wie viel er geben kann.
Wir haben diese <a href="https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee">Liste</a> gefunden mit Kaffeepreisen rund um die Welt. Die Liste ist vielleicht nicht hundertprozentig korrekt oder gar tagesaktuell, aber Du hast in ihr einen guten Indikator für die verschiedenen Preise.
<br><br>
<strong>Nimm Dir ruhig Zeit, die Höhe Deines Beitrags abzuwägen.</strong> Wenn Du uns einen Rio de Janeiro-Kaffee im Monat "kaufen" kannst, ist das OK. Wenn Du Dir jedoch einen Doppelten entkoffeinierten Soja Frappuccino mit einem extra Shot und Sahne im Monat leisten kannst, dann hilfst Du uns wirklich, die Disroot-Plattform am Laufen zu halten und sicherzugehen, dass sie frei zugänglich bleibt für andere Menschen mit weniger Mitteln.</p>
