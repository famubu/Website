---
title: 'Der Untergang von Diaspora'
media_order: tunnel.jpg
published: true
date: '19-01-2020 18:00'
taxonomy:
    category:
        - news
    tag:
        - disroot
        - news
        - roadmap
body_classes: 'Demise of Diaspora'
---

Im letzten Jahr hat unser Interesse an **Diaspora** als Lösung für ein soziales Netzwerk stark abgenommen. Wir wussten, dass irgendwann die Zeit kommen wird, in der wir eine Entscheidung über die Zukunft unseres Pods auf der föderierten Landkarte treffen müssen. Nun ist der Moment gekommen, dem Unausweichlichen zu begegnen und anzukündigen, dass wir **Diaspora** auslaufen lassen zugunsten von etwas, das nach unserem Gefühl ein größeres Potential besitzt, besser zu unserer Plattform passt und bei dem wir uns im letzten Jahr engagiert haben.

Die Performance unseres **Diaspora**-Pods war in den letzten Monaten schrecklich. Die Leute haben sich ständig über langsame Stream-Ladezeiten beschwert und wir waren auch persönlich nicht zufrieden mit der Performance, obwohl wir getan haben, was wir konnten, um sie zu verbessern. Wir wurden außerdem durch die Spam-Accounterstellung angegriffen, was uns zu der Entscheidung brachte, die Registrierung temporär zu schließen, während wir dagegen ankämpften. Allerdings haben wir die Registrierung nie wieder geöffnet, was leider unseren Mangel an Motivation zur Aufrechterhaltung des Pods widerspiegelt.

Haben wir irgendetwas gegen **Diaspora**? Mitnichten.

Natürlich könnte die Performance verbessert werden. **Diaspora** war von Anfang an dabei. Es war **Diaspora**, wo wir als erstes ein "Hallo Welt" als **Disroot**-Projekt sendeten, tatsächlich war es die eine und einzige Werbung für **Disroot** durch uns. Es war eine großartige Erfahrung und eine großartige Gemeinschaft. Indes bleibt nichts am selben Platz. Zeiten ändern sich (*"Das Ökosystem bewegt sich"* :wink: :wink:) und wir haben das Gefühl, wir müssen weitergehen. Wir wollen nicht in die Situation geraten, mehrere soziale Netzwerke anzubieten, und müssen daher eine einzige Lösung wählen, hinter der wir stehen, auf die wir den Fokus legen und die wir unterstützen können so viel es geht. In den letzten vier Jahren war dies **Diaspora**, aber wenn wir uns umsehen, gibt es da phantastische Projekte, die unser Interesse geweckt haben und in die wir uns tiefer eingearbeitet haben. Die überwältigende Ausstattung an Funktionalitäten von **Hubzilla** betreffend nicht nur die Interoperabilität mit anderen Protokollen sondern auch Funktionalitäten wie Gruppen, Profile, Chats, Foren, Artikel und natürlich die nomadische Identität und das Zot-Protokoll im Allgemeinen haben uns mehr und mehr in diese Richtung blicken lassen. Auf der anderen Seite zeigen die ActivityPub-betriebenen Netzwerke das Potential neuer, frischer und massiv beschäftigter, föderativer Netzwerke. Wir spüren den Änderungsbedarf und wollen unsere Anstrengungen in etwas stecken, mit dem wir uns identifizieren können. Offensichtlich setzen wir uns gerade für **Hubzilla** ein, weil wir glauben, dass es ein brillantes soziales Netzwerk ist, das seiner Zeit voraus ist, eine nirgendwo sonst zu findende Ausstattung an Funktionalitäten bietet und dennoch unterschätzt wird. Nichtsdestoweniger wollen wir keine "impulsiven" Entscheidungen treffen. Deshalb können wir zu diesem Zeitpunkt noch nicht sagen, welches soziale Netzwerk wir schlussendlich tatsächlich auswählen werden. Wahrscheinlich wird es **Hubzilla**, aber es könnte ebensogut Friendica, Pleroma, Pixelfed oder sogar Mastodon werden. Zum jetzigen Zeitpunkt ist es noch zu früh, um das zu sagen. Wir wissen, dass **Hubzilla** seine Defizite hat und bevor wir es nicht geschafft haben, uns mit diesen zu befassen, bleibt die Frage unbeantwortet.

Was wir sagen können, ist, dass wir unseren aktuellen Diaspora-Pod auslaufen lassen. Was bedeutet das für aktuelle Nutzer? Eigentlich nicht viel. **Diaspora** wird operativ bleiben, bis der letzte Nutzer die Lichter ausmacht. Wir sind nicht **Google**, die die Leute rausschmeißen wie bei **G+**. Wir werden jedoch:

-   Die **Diaspora**-Seite von unserer Website entfernen,
-   die Registrierung schließen,
-   mit der Bereinigung inaktiver Accounts ohne die Möglichkeit der Reaktivierung beginnen. Wir werden automatisch und ohne vorherige Warnung Nutzer entfernen, die seit mehr als 4 Monaten inaktiv sind.
-   In der Zwischenzeit werden wir an Diasporas Ersatz arbeiten.

Wir möchten uns bei den Entwicklern von Diaspora bedanken, dass Ihr uns das erste föderative soziale Netzwerk überhaupt geboten habt, für all die Jahre, die Ihr bei uns wart während und vor der Geburt von Disroot. Wir sind Euch ungemein dankbar für all Euren Einsatz und Eure Arbeit. Wir werden unser Bestes tun, das Protokoll der "Föderation" nicht zu verlassen, und wir werden weiterhin im Kontakt mit dem Diaspora-Netzwerk bleiben. Für erste aber sagen wir: Macht's gut und danke für den Fisch. Wir sehen uns auf der anderen Seite.
