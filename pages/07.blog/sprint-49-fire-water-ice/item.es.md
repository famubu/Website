---
title: 'Fuego, agua y hielo'
date: '09-12-2019 15:00'
media_order: 49104435191_596d016b7b_k.jpg
taxonomy:
  category: news
  tag: [disroot, nextcloud, issues, hoja de ruta, sprint, burnout]
body_classes: 'single single-post'

---
*[Sprint 49 - Changement de saison]*

¡Qué montaña rusa fueron las últimas semanas!


Empezamos con un **fuego** gigantesco que ha estado devorándose nuestro servidor durante las últimas dos semanas y media. Todo fue provocado por la actualización de PHP (el backend que impulsa a nuestro servicio de Nextcloud). Sabíamos que era problemático, porque ya hemos observado la anomalía en el pasado, y decidimos retrotraerlo a la versión previa. Aunque incluso ya habíamos tomado recaudos para proteger nuestra configuración del servidor web, una vulnerabilidad recientemente descubierta y los rumores de un ransomware suelto que cifra instancias enteras, nos convenció de seguir adelante con la actualización del PHP subyacente - solo para estar en terreno seguro. Esto, como esperábamos, dio como resultado que nuestro servidor trabajara a su máxima potencia, causando que toda la nube se pusiera lenta hasta arrastrarse. Después de muchas pruebas, modificaciones, días y noches sin dormir tratando de encontrar la causa principal, la identificamos y le aplicamos un arreglo temporal. Al mismo tiempo, comunicamos esto al equipo de desarrollo de Nextcloud y ahora estamos esperando una solución definitiva. El **agua** tan necesitada extinguió el fuego y trajo de nuevo estabilidad a la plataforma.

Todo esto, y el hecho de que nuestras responsabilidades por fuera de Disroot, como nuestros trabajos diarios, familias y otros proyectos, se volvieran más demandantes, nos hizo dar cuenta que mantener el ritmo actual de trabajo constante - el ciclo continuo de sprints de dos semanas, mejorar ininterrumpidamente la plataforma, responder a todos los tickets de soporte, las solicitudes, mantener conversaciones con ustedes a través del chat, redes sociales y lo que sea - definitivamente no puede continuar en el presente escenario o nos va a llevar al agotamiento total en el futuro cercano. A menos que Disroot se convierta en nuestra principal ocupación, lo que de hecho ya lo es porque algunxs de nosotrxs pasa más tiempo trabajando en ella que en sus trabajos diarios, necesitamos un balde de **hielo** y bajar un poco la velocidad.

Así que decidimos extender el período de tiempo de los sprints de 2 semanas a 4. Por supuesto, esto significa que aún seguiremos trabajando constantemente en Disroot, pero con la prolongación de dos semanas adicionales pensamos que se sentirá menos como "el día de la marmota" - con reuniones semanales, inicio/finalización de sprints, refinamientos volviéndose un largo ciclo constante pateándote el culo cada semana (ahora son solo quincenales :P). Sabemos que en el futuro próximo necesitamos enfocarnos más en la sustentabilidad económica más allá de simplemente pagar los costos de hospedaje y juntar dinero para actualizaciones de hardware. Necesitamos crear un ecosistema en el que podamos compromenternos con Disroot, el software libre y de código abierto y una internet descentralizada y federada como nuestra principal ocupación si vamos a continuar con esto. Tenemos algunas ideas gestándose y seremos más comunicativos respecto a ellas el próximo año.

## Lo que viene...

Este primer sprint de 4 semanas se llama *"Subiendo al tren de esa hoja de ruta"* y nos estaremos enfocando en terminar los puntos más importantes de la hoja de ruta actual (Q4 2019) para empezar el nuevo año con mejor organización, una incorporación y experiencia de usuarix mejorada, mentes e ideas claras sobre lo que próximo que haremos.
