---
title: 'Concorso creazione adesivi di fine anno'
media_order: stickers.jpg
published: true
date: '07-12-2020 21:00'
taxonomy:
    category:
        - news
    tag:
        - news
        - contest
body_classes: 'single single-post'
---

Dopo la raccolta fondi dell'anno scorso che ha avuto un grande successo, abbiamo pensato di chiudere questo pazzo anno con una proposta divertente e creativa. È per questo che abbiamo deciso di proporre a tutti i Disrooter un contest nel quale siano gli utenti stessi a progettare e disegnare gli adesivi di Disroot.

L'obiettivo è semplice. **Crea l'adesivo e caricalo [qui](https://cloud.disroot.org/s/FP9HPEXbfKb6ZQQ) prima del 25 dicembre**. 
Gli adesivi più belli verranno stampati e inviati a chi supporterà Disroot con delle donazioni durante l'anno 2021.

Organizzeremo una votazione, in questo modo chiunque potrà votare gli adesivi che ritiene più belli. Ai vincitori, in segno di gratitudine e come regalo di nuovo anno, verranno regalati alcuni degli adesivi stampati;)

Qui alcune specifiche:
* Sii creativo;)
* Gli adesivi non devono essere specifici per Disroot. Puoi anche creare adesivi relativi al mondo FLOSS o al fediverso (siamo tutti una famiglia)
* Invia il file preferibilmente in formato svg (in alternativa in formato png)
* Fuck 2020 (ecco, anche questo slogan ci piace;))

La licenza con la quale verranno rilasciati le opere;) sarà Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

Quindi prendi la tua tavoletta grafica, il mouse, una penna o qualunque cosa ti serva e disegna l'adesivo Disroot più bello!
Non vediamo l'ora di vedere i progetti!

Non esitare e condividi il concorso con l'hashtag **#Distickeroot** sui tuoi social network preferiti!

**Il team Disroot**
