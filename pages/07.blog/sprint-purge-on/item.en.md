---
title: 'Sprint - Purge On'
date: 09/11/2018
media_order: purge.jpg
taxonomy:
  category: news
  tag: [disroot, news, matrix, sprint]
body_classes: 'single single-post'

---

Monday we have started new sprint.
Last sprint, was not as successful as we planned it to be. Due to personal issues, and the whole [shitshow with abusers](https://disroot.org/en/blog/temporary-closed-registration), we could not finish things we've planned. We did however, purge lots of abusive accounts, sent out the [disroot-matrix closure announcement](https://disroot.org/en/blog/matrix-closure) and tried matrix history room purging (which failed in spectacular 24h matrix downtime). On the good note we've sorted out the mailbox size counter on webmail so you can finally see your mailbox size 🤩:

![](mailboxsize.png)


In coming two weeks we want to put our focus on:

1. **Disroot Signup problem**
In the coming two weeks we will do lots of thinking, brainstorming and discussing the best way to re-enable registrations on disroot, while at the same time preventing the abuse we've dealt with in the last months. Since we are getting a lot of suggestions on all communication platforms and we want to share our thoughts, we've created a central place in the form of a forum thread where we will keep updating and discussing the matter. You can find it [here](https://forum.disroot.org/t/how-to-solve-the-abuse-problem-on-disroot-org-platform)

1. **Creating mockups for conversejs**
(currently our jabber webchat of choice https://webchat.disroot.org ) We want to work on a theme and extra set of features to make the webchat shine. We hope some of the community members will jump onboard and help us out too

2. **Jabber/XMPP improvements**
In order to be fully compliant with [xmpp compliance list](https://compliance.conversations.im/) we need to setup additional modules such as push notifications for iOS users and XMPP over TLS-443 port.

3. **Possibility to change screen name and notification email address**
Users should be able to choose and change their screennames and those should reflect accross all services (if possible). Additionally in order to not force everyone to use our email (eg. not every cloud user wants to use disroot mail), we want to make it possible to change the email address to which notifications are going to. This will prevent the need to setup forwarding filter rules on webmail.

4. **Purge continues**
We will continue trying to purge matrix room history to regain disk space on our database server. We have new ideas how to approach this issues and we hope (if all works well) with minimum downtime we will manage to purge all public rooms history down to last 4 months.

5. **New financial overview page**
Our current donation page is quite outdated. We are thinking on a better approach to display current financial state and costs to give better overview on what is happening to all people.

**Extras**
If for some reason coffee will keep us awake or we get fired form our dayjobs, we hope to put some love into the beetroot hubzilla theme.
