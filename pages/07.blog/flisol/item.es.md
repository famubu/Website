---
title: 'FLISoL 2020'
media_order: flisol_2020.jpg
published: true
date: '14-04-2020 00:30'
taxonomy:
    category:
        - noticias
    tag:
        - flisol
        - novedades
body_classes: 'single single-post'

---
Las **Comunidades Latinoamericanas de Tecnologías Libres** invitan a todxs a una intensa jornada de  charlas, talleres e instalaciones en vivo.

El sábado 25 de abril de 2020, a partir de las 09:00 (UTC-5) se llevará acabo una nueva edición del **FLISoL**, el **Festival Latinoamericano de Instalación de Software Libre**, el mayor evento de difusión y promoción del Software Libre que se realiza desde el año 2005 en diferentes países y de manera simultánea.

Está dirigido a todas las personas interesadas en conocer más acerca del software y la cultura libre. En el encuentro, las diversas comunidades locales de software libre, organizan eventos en los que se instala, de manera legal y gratuita, software libre en las computadoras que llevan los asistentes. Paralelamente, hay charlas, ponencias y talleres, sobre temáticas locales, nacionales y latinoamericanas en torno al Software Libre, en toda su gama de expresiones: artística, académica, empresarial y social.

Este año, como consecuencia del brote de Coronovirus, el Festival será íntegramente en línea.

Para más información, y registrarse de manera libre y gratuita, ve a:
[**flisolonline.softlibre.com.ar**](https://flisolonline.softlibre.com.ar)
