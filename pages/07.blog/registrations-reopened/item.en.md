---
title: 'Registrations re-opened!'
date: 10/26/2018
media_order: poland-communist-reality.jpg
taxonomy:
  category: news
  tag: [disroot, news, registration]
body_classes: 'single single-post'

---


As we mentioned in the previous post, for the last two plus weeks we've been busy working on getting the registrations back up and running. We hope the process is not too complex and that we will manage to cope with the extra workload the manual approval process brings. We've written bunch of tools that should help us make this process as easy and fast as we possibly can. We will be reviewing new user requests once a day so prepare for few hours of wait time when creating new user.
Lets all hope this process will appear to be working well and we won't be forced to rethink the whole approach again. Time will tell.

Automated delete account process.
While working and thinking on new user registration system, we thought of a way to turn current delete account system to fully automated one. Currently user deletion system is pretty much a manual work, where we remove user accounts and data on weekly basis. This is hardly ideal. Users had to fill in the form, reply to email etc. and since we had to do the actual deletion manually, we often (usually) lagged with the process as more important things came first. From now on, you can finally remove your account via [https://user.disroot.org](https://user.disroot.org) - your account will be disabled immediately and all the data will be removed within 48h automatically. You can already use it, though the auto-deletion of user data will be in place in coming days as we haven't finish it entirely yet.

**What's planned for next two weeks?**
Next weeks we want to focus on few service updates which require more custom work then usual:

Hubzilla just got a pretty big update, changing the approach from fairly complex settings system to much easier one to use. This means we need to test it and make sure our pre-defined settings for new users stay intact.

Taiga and Discourse needs quite some work as the updates are blocked by manual custom changes we need to apply in order to get them to work. Something we wanted to automate for a long time and we just simply cant postpone it any longer.

Nextcloud needs a lot of love. For last months we've observed quite a jump in Nextcloud usage which also means amount of resources needed. Apart from updating Nextcloud to version 14, we want to start working on some improvements, optimizations and changes that will allow for smoother performance and most importantly lower resource footprint.

cheers!
